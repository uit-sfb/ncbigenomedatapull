# NCBIGenomeDataPull
Python script for pulling genome data from the NCBI Database.

# General info
Script uses a number of packages from Biopython, such as Entrez and SeqIO to pull and parse data from NCBI.

# How to use
The following line demonstartes the example of running the script:
```
python script.py -e <email> -i <input file> -o <output directory for assembly data> -p <output directory for protein data> -n <output directory for CDS nucleotide data> -m <output path for metadata csv table> -check <path to reference .tsv file>
```
The email address is optional and needed for the Entrez API to notify user if the amount of calls
is too big and the API is being overused.

The input should be a csv file where the first column is the accession ids

If csv table with metadata already exists, script will add new entries to this table using reference file or create new csv document if no table exists. 

In order to check for entries which were already processed, the path to reference file needs to be specified. Script will write the content of "current.tsv" from existing Metadatadb to the specified reference file and use this file to check the entries from the input.

# Output files
The output of the script consists of 4 types of files:
* Sequence data for the individual entry in fasta format
* CDS Protein features data for the individual entry in fasta format, which includes translation for each individual gene and CDS info from the genbank
* CDS Nucleotide data for the individual entry in fasta format which includes individual nucleotides and CDS data for them
* Metadata from all processed entries, stored in .csv table

