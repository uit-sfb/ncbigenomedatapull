from Bio import Entrez,SeqIO
import sys, argparse, os
import csv
import re
from pandas import read_csv
import urllib.request

def writeSeqFasta(record,filePath):
    outSeq = os.path.join(str(filePath) + "/","sequences_{}.fasta".format(record.id)) if filePath else "sequences_{}.fasta".format(record.id)
    removeTabs = lambda s: " ".join(s.split('\t'))
    if not os.path.exists(outSeq):
        with open(outSeq,"w") as out_seq:
            for line in record.format('fasta'):
                out_seq.write(removeTabs(line))


def getTSVInput(inputPath):
    df = read_csv(inputPath,sep='\t',engine='python')
    res = df['genbank_accession'].tolist()
    accList = [acc for acc in res if (acc and str(acc) != 'missing')]
    return accList

def getCSVInput(inputPath):
    df = read_csv(inputPath)
    res = df['genbank_accession'].tolist()
    accList = [acc for acc in res if (acc and str(acc) != 'missing')]
    return accList

def main(argv):
    parser = argparse.ArgumentParser(description='Set email and max amount of results')
    parser.add_argument('-e',dest='email',default='',help='email')
    parser.add_argument('-i',dest='inputFile',default='',help='input file')
    parser.add_argument('-o',dest='out',default='',help='output dir for assembly')
    parser.add_argument('-p',dest='outProt',default='',help='output dir for protein')
    parser.add_argument('-n',dest='outNuc',default='',help='output dir for nucleotide')
    parser.add_argument('-m',dest='outMeta',default='',help='output dir for metadata')

    args = parser.parse_args()
    accessionList = getTSVInput(args.inputFile)
    Entrez.email = args.email
    return accessionList, args.out, args.outProt, args.outNuc, args.outMeta;

def writeFeaturesToFile(record,filePath):
    gb_features = record.features
    feats = [feat for feat in gb_features if feat.type == "CDS"]
    outFeat = os.path.join(str(filePath) + "/","sequences_{}_protein.fasta".format(record.id)) if filePath else "sequences_{}_protein.fasta".format(record.id)
    removeTabs = lambda s: " ".join(s.split('\t'))
    counter = 0
    if not os.path.exists(outFeat):
        with open(outFeat,"w") as out_handle:
            for feat in feats:
                counter += 1
                p = re.compile("\[(.*)\]")
                res = str(feat.location).split(',')
                res_lst = [ "{}:".format(record.id) + "..".join(p.search(s).group(1).split(':')) for s in res]
                if len(res_lst) > 1:
                    last_res = "join({})".format(",".join(res_lst))
                else:
                    last_res = res_lst[0]
                location = "[{}={}]".format("location",last_res)
                type = "[{}={}]".format("type",str(feat.type))
                qual_str = " ".join(["[{}={}]".format(k, v[0]) for k,v in feat.qualifiers.items() if (v[0] and k != "translation")])
                translation_str = feat.qualifiers["translation"][0]
                mainId = str(record.id)
                dataLine = " ".join([">{}_{}".format(mainId,counter),location,type,qual_str])
                out_handle.write(removeTabs(dataLine) + "\n" + removeTabs(translation_str) + "\n")

def writeMetadataToFile(record,filePath):
    accession = record.id
    gen_length = len(record.seq)
    definition = record.description
    comm = record.annotations.get('structured_comment',{})
    assembly_data = comm.get('Assembly-Data',{})
    assembly_method = ",".join(assembly_data.get('Assembly Method',"").split(';'))
    coverage = assembly_data.get('Coverage',"")
    sequencing_technology = ",".join(assembly_data.get('Sequencing Technology',"").split(';'))
    outPath = filePath if filePath else "metadata.csv"

    gb_features = record.features
    feats = [feat for feat in gb_features if feat.type == "source"]
    feat_data = feats[0].qualifiers if feats else {}
    strain = feat_data.get('strain',[])
    isolate = feat_data.get('isolate',[])
    isolation_source = feat_data.get('isolation_source',[])
    host = feat_data.get('host',[])
    country = feat_data.get('country',[])
    collection_date = feat_data.get('collection_date',[])
    getItem = lambda l: l[0] if l else ""
    removeColumn = lambda s: ",".join(s.split(';'))

    with open(outPath,'a', newline='') as csvout:
        fieldnames = ["accession","genome_length","definition","assembly_method","coverage","sequencing_technology","strain","isolate","isolation_source","host","country","collection_date"]
        writer = csv.DictWriter(csvout,fieldnames=fieldnames)
        writer.writerow({'accession':accession,'genome_length':gen_length,'definition':definition,'assembly_method':assembly_method,'coverage':removeColumn(coverage),'sequencing_technology':sequencing_technology,"strain":removeColumn(getItem(strain)),"isolate":removeColumn(getItem(isolate)),"isolation_source":removeColumn(getItem(isolation_source)),"host":removeColumn(getItem(host)),"country":removeColumn(getItem(country)),"collection_date":removeColumn(getItem(collection_date))})

def getFileSize(url):
    site = urllib.request.urlopen(url)
    return len(site.read())

def pullReferenceFile(path):
    url = "https://s1.sfb.uit.no/public/mar/SarsCoV2DB/Metadatabase/current.tsv"
    fileSize = getFileSize(url)
    if not os.path.exists(path):
        print("pulling reference file")
        urllib.request.urlretrieve(url, path)
    else:
        if fileSize != os.path.getsize(path):
            print("file size is different, download new file from SarsCoV2DB, remove {}".format(path))
            os.remove(path)
            urllib.request.urlretrieve(url, path)
        else:
            print("file already exists")

def getCDSData(record,filePath):
    feats = [feat for feat in record.features if feat.type == "CDS"]
    organism = record.annotations['organism'] #defines your organism ID
    path = os.path.join(filePath,"sequences_{}_cds.fasta".format(record.id))
    removeTabs = lambda s: " ".join(s.split('\t'))
    counter = 0
    if not os.path.exists(path):
        with open(path,'w') as out_f:
            for feat in feats:
                counter += 1
                p = re.compile("\[(.*)\]")
                res = str(feat.location).split(',')
                res_lst = [ "{}:".format(record.id) + "..".join(p.search(s).group(1).split(':')) for s in res]
                if len(res_lst) > 1:
                    last_res = "join({})".format(",".join(res_lst))
                else:
                    last_res = res_lst[0]
                location = "[{}={}]".format("location",last_res)
                mainId = ">{}_{}".format(record.id,counter)
                product = feat.qualifiers.get("product",[""])
                data = " ".join([mainId,location,"[{}={}]".format("product",str(product[0])),"[{}={}]".format("organism",str(organism))]) if product[0] else " ".join([mainId,location,"[{}={}]".format("organism",str(organism))])
                out_f.write(removeTabs(data) + '\n' + removeTabs(str(feat.location.extract(record).seq)) + '\n')

def writeCSVHeader(filePath):
    outPath = filePath if filePath else "metadata.csv"
    with open(outPath,'w', newline='') as csvout:
        fieldnames = ["accession","genome_length","definition","assembly_method","coverage","sequencing_technology","strain","isolate","isolation_source","host","country","collection_date"]
        writer = csv.DictWriter(csvout,fieldnames=fieldnames)
        writer.writeheader()


def pullRecordData(id):
    try:
        handle = Entrez.efetch(db="Nucleotide", id=id, rettype="gb", retmode="text")
        record = SeqIO.read(handle, "genbank")
        handle.close()
        return record
    except urllib.error.URLError as e:
        print("{} not fetched, reason: {}".format(id,e.reason))
        return ""

if __name__ == "__main__":
   records, path, prot, nuc, pathMeta = main(sys.argv[1:])
   filePath = path
   filePathNuc = nuc
   filePathProt = prot
   metadataPath = pathMeta
   records = list(set(records))
   if not os.path.exists(metadataPath):
       writeCSVHeader(metadataPath)
   for recordId in records:
       record = pullRecordData(recordId)
       if record:
           writeMetadataToFile(record,metadataPath)
           writeSeqFasta(record,filePath)
           writeFeaturesToFile(record,filePathProt)
           getCDSData(record,filePathNuc)
