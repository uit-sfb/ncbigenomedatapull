FROM python:3

ARG version

LABEL maintainer="mmp@uit.no" \
  toolVersion=$version \
  toolName=ncbigenomedatapull \
  url=https://gitlab.com/uit-sfb/ncbigenomedatapull

WORKDIR /app/ncbigenomedatapull

COPY src/requirements.txt ./

RUN pip install --no-cache-dir -r requirements.txt

COPY src/script.py ./

ENTRYPOINT [ "python", "./script.py" ] 
